#!/bin/sh

tfile=`mktemp`
cat > $tfile << EOF
USE mysql;
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY "$MYSQL_ROOT_PASSWORD" WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' WITH GRANT OPTION;
ALTER USER 'root'@'localhost' IDENTIFIED BY '';
EOF

mkdir -p /var/run/mysqld
chown -R mysql:mysql /var/run/mysqld
/usr/bin/mysql_install_db
/usr/bin/mysqld --user=root --bootstrap < $tfile
rm -f $tfile
/usr/bin/mysqld_safe


# mkdir -p /var/run/mysqld
# chown -R mysql:mysql /var/run/mysqld
# mysql_install_db --user=root --datadir=/data
# /usr/bin/mysqld --user=root --bootstrap < $tfile
# rm -f $tfile
# exec /usr/bin/mysqld --user=root --console
